use chrono::{Datelike, Duration, NaiveDate, Utc};
use decimal::d128;
use lru::LruCache;
use std::collections::HashMap;

const CURRENCY_CODES: &'static [&'static str] = &[
    "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT",
    "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BYN", "BYR",
    "BZD", "CAD", "CDF", "CHE", "CHF", "CHW", "CLF", "CLP", "CNY", "COP", "COU", "CRC", "CUC",
    "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD", "FKP",
    "GBP", "GEL", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF",
    "IDR", "ILS", "INR", "IQD", "IRR", "ISK", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF",
    "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL",
    "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZN",
    "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN",
    "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP",
    "SLL", "SOS", "SRD", "SSP", "STD", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP",
    "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "USN", "UYI", "UYU", "UZS", "VEF", "VND",
    "VUV", "WST", "XAF", "XCD", "XDR", "XOF", "XPF", "XSU", "XUA", "YER", "ZAR", "ZMW", "ZWL",
];

pub struct Cache {
    n_days: usize,
    data: Vec<Option<Vec<Option<d128>>>>,
    currency_codes: HashMap<String, u8>,
    lru: LruCache<(String, NaiveDate), d128>,
    len: usize,
}

impl Cache {
    pub fn new(n_days: usize) -> Cache {
        let codes: HashMap<String, u8> = CURRENCY_CODES
            .iter()
            .enumerate()
            .map(|x| (x.1.to_string(), x.0 as u8))
            .collect();

        Cache {
            n_days: n_days,
            currency_codes: codes,
            data: vec![None; CURRENCY_CODES.len()],
            lru: LruCache::new(120),
            len: 0,
        }
    }

    pub fn len(&self) -> usize {
        self.len + self.lru.len()
    }

    pub fn set(&mut self, currency: String, date: NaiveDate, rate: d128) {
        if self.is_out_of_bound(&currency, &date) {
            self.lru.put((currency, date), rate);
            return;
        }

        let date_idx = self.get_date_idx(&date);
        let currency_idx = self.get_currency_idx(&currency);

        if self.data[currency_idx].is_none() {
            self.data[currency_idx] = Some(vec![None; self.n_days]);
        }

        if let Some(dates) = self.data[currency_idx].as_mut() {
            dates[date_idx] = Some(rate);
        }
    }

    pub fn get(&mut self, currency: &str, date: &NaiveDate) -> Option<&d128> {
        if self.is_out_of_bound(currency, date) {
            let key = (currency.to_string(), date.clone());
            return self.lru.get(&key);
        }

        let currency_idx = self.get_currency_idx(currency);

        if let Some(dates) = self.data[currency_idx].as_ref() {
            let date_idx = self.get_date_idx(date);

            if let Some(rate) = dates[date_idx].as_ref() {
                return Some(rate);
            }
        }

        None
    }

    // pub fn get_many(&mut self, currency: &str, dates: Vec<&NaiveDate>) -> Vec<Option<&d128>> {
    //     let currency_idx = self.get_currency_idx(currency);

    //     let mut result = vec![None; dates.len()];

    //     if let Some(dd) = self.data[currency_idx].as_ref() {
    //         for (i, date) in dates.into_iter().enumerate() {
    //             let date_idx = self.get_date_idx(date);

    //             if self.is_out_of_bound(currency, date) {
    //                 let key = (currency.to_string(), date.clone());
    //                 result[i] = self.lru.get(&key);
    //             } else if let Some(rate) = dd[date_idx].as_ref() {
    //                 result[i] = Some(rate);
    //             }
    //         }
    //     }
    //     result
    // }

    pub fn contains(&mut self, currency: &str, date: &NaiveDate) -> bool {
        if self.is_out_of_bound(currency, date) {
            let key = (currency.to_string().clone(), date.clone());
            return self.lru.contains(&key);
        }

        let currency_idx = self.get_currency_idx(currency);

        if let Some(dates) = self.data[currency_idx].as_ref() {
            let date_idx = self.get_date_idx(date);
            return dates[date_idx].as_ref().is_some();
        }

        false
    }

    fn is_out_of_bound(&self, currency: &str, date: &NaiveDate) -> bool {
        let cutoff_date = Utc::today().naive_utc() - Duration::days(self.n_days as i64);
        (date < &cutoff_date) || !self.currency_codes.contains_key(currency)
    }

    fn get_date_idx(&self, date: &NaiveDate) -> usize {
        (date.num_days_from_ce() as usize) % self.n_days
    }

    fn get_currency_idx(&self, currency: &str) -> usize {
        *self.currency_codes.get(currency).unwrap() as usize
    }
}
