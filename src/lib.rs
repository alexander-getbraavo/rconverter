mod cache;
use crate::cache::Cache;
use chrono::NaiveDate;
use decimal::d128;
use pyo3::class::{PyMappingProtocol, PySequenceProtocol};
use pyo3::exceptions::KeyError;
use pyo3::gc::{PyGCProtocol, PyTraverseError, PyVisit};
use pyo3::prelude::*;
use pyo3::types::{PyAny, PyDate, PyDateAccess, PyString, PyTuple};
use std::cell::RefCell;

#[pyclass]
struct PyCache {
    cache: RefCell<Cache>,
    decimal_class: Option<PyObject>,
}

#[pymethods]
impl PyCache {
    #[new]
    fn new(obj: &PyRawObject, size: usize) {
        let gil = Python::acquire_gil();
        let py = gil.python();
        let decimal_module = PyModule::import(py, "decimal").unwrap();
        let decimal_class = decimal_module.get("Decimal").unwrap().to_object(py);

        obj.init(Self {
            cache: RefCell::new(Cache::new(size)),
            decimal_class: Some(decimal_class),
        })
    }

    fn get_rate(&self, currency_from: &str, currency_to: &str, date: &PyDate ) -> PyResult<Option<PyObject>> {
        let date = NaiveDate::from_ymd(
            date.get_year(),
            date.get_month() as u32,
            date.get_day() as u32,
        );

        if let Some(rate_1) = self.cache.borrow_mut().get(&currency_from, &date) {
            if let Some(rate_2) = self.cache.borrow_mut().get(&currency_to, &date) {
                let rate = rate_2 / rate_1;
                return Ok(Some(self.as_pydecimal(&rate)?));
            }
        }

        Ok(None)
    }
}

#[pyproto]
impl PyGCProtocol for PyCache {
    fn __traverse__(&self, visit: PyVisit) -> Result<(), PyTraverseError> {
        if let Some(ref obj) = self.decimal_class {
            visit.call(obj)?
        }
        Ok(())
    }

    fn __clear__(&mut self) {
        if let Some(obj) = self.decimal_class.take() {
            // Release reference, this decrements ref counter.
            let gil = GILGuard::acquire();
            let py = gil.python();
            py.release(obj);
        }
    }
}

impl PyCache {
    fn deconstruct(key: &PyTuple) -> PyResult<(NaiveDate, String)> {
        let date: &PyDate = key.get_item(0).downcast_ref()?;
        let date = NaiveDate::from_ymd(
            date.get_year(),
            date.get_month() as u32,
            date.get_day() as u32,
        );

        let currency: &PyString = key.get_item(1).downcast_ref()?;
        let currency = currency.to_string()?.into_owned();

        Ok((date, currency))
    }

    fn as_pydecimal(&self, value: &d128) -> PyResult<PyObject> {
        let gil = Python::acquire_gil();
        let py = gil.python();
        let dec = self.decimal_class.as_ref().unwrap().call1(py, (value.to_string(),))?;
        Ok(dec.to_object(py))
    }
}

#[pyproto]
impl PySequenceProtocol for PyCache {
    fn __contains__(&self, key: &PyTuple) -> PyResult<bool> {
        let (date, currency) = Self::deconstruct(key)?;
        Ok(self.cache.borrow_mut().contains(&currency, &date))
    }
}

#[pyproto]
impl PyMappingProtocol for PyCache {
    fn __getitem__(&self, key: &PyTuple) -> PyResult<PyObject> {
        let (date, currency) = Self::deconstruct(key)?;

        if let Some(rate) = self.cache.borrow_mut().get(&currency, &date) {
            return self.as_pydecimal(&rate);
        }

        KeyError::py_err(key.str()?.to_string()?.into_owned()).into()
    }

    fn __setitem__(&mut self, key: &PyTuple, value: &PyAny) -> PyResult<()> {
        let value = value.str()?.to_string()?.into_owned();
        let (date, currency) = Self::deconstruct(key)?;
        self.cache
            .borrow_mut()
            .set(currency, date, value.parse::<d128>().unwrap());

        Ok(())
    }

    fn __len__(&self) -> PyResult<usize> {
        Ok(self.cache.borrow().len())
    }
}

#[pymodule]
fn rconverter(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<PyCache>()?;

    Ok(())
}
